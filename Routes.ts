//
//  Routes.ts
//  Climate API
//
//  Created on October 15, 2016 by Animesh.
//  Copyright (c) 2016, Greenhill Sustainability
//  All rights reserved.
//

import * as Express from "express"
import { AppInfo } from "./controller/AppInfo"
import { CloudantClient } from "./controller/CloudantClient"
import { Letterpress } from "./controller/Letterpress"

export class Routes
{
    private static _appInfo: AppInfo
    private static _cloudant: CloudantClient

    public static Configure(app: Express, details: AppInfo, dbClient: CloudantClient) 
    {
        Routes._appInfo = details
        Routes._cloudant = dbClient

        app.get("/", Routes.home)
        app.all("/api/:version/*", Routes.versionCheck)
        app.get("/api/:version/:locID/:period/:tempAvg/:variable/:emissions", Routes.getProjection)
        app.post("/api/:version/query", Routes.query)
    }

    private static home(request: Express.Request, response: Express.Response, next: Express.NextFunction) 
    {
        let homePage = Letterpress.HomePage(Routes._appInfo)
        response.setHeader("Content-Type", "text/html")
        response.status(200).end(homePage)
    }

    private static versionCheck(request: Express.Request, response: Express.Response, next: Express.NextFunction) 
    {
        if (Routes._appInfo.DoesSupport(request.params.version)) {
            next()
        }
        else {
            let message = "This version of the API is not available publicly. Please try " + Routes._appInfo.version + " instead."
            let error = {
                name: "Error",
                error: message
            }
            response.setHeader("Content-Type", "application/json")
            response.status(403).end(JSON.stringify(error))
        }
    }

    private static getProjection(request: Express.Request, response: Express.Response, next: Express.NextFunction) 
    {
        let batch: string
        if (request.params.variable.toLowerCase() == "slp") {
            batch = "bat2"
        }
        else {
            batch = "bat1"
        }

        let id = batch + "-abs-" + request.params.variable + "-" +
                 request.params.emissions + "-" +  
                 request.params.period + "-" +
                 request.params.tempAvg + "-25km-" +
                 request.params.locID + "-samp"
        
        Routes._cloudant.getDocumentWithID(id.toLowerCase(), (data?: any, error?: any) => {
            response.setHeader("Content-Type", "application/json")
            if(error) {
                response.status(error.statusCode).end(JSON.stringify(error))
            }
            else {
                response.status(200).end(JSON.stringify(data))
            }
        })
    }

    private static query(request: Express.Request, response: Express.Response, next: Express.NextFunction) 
    {
        let selector = request.body
        console.log(selector)
        Routes._cloudant.search(selector, (data?: any, error?: any) => {
            console.log(error)
            console.log(data)
            response.setHeader("Content-Type", "application/json")
            if(error) {
                response.status(404).end(JSON.stringify(error))
            }
            else {
                if (data.docs) {
                    if (data.docs.length > 0) {
                        response.status(200).json(data.docs)
                    }
                    else {
                        response.status(404).json("Couldn't find any documents. Docs count = 0")
                    }
                }
                else {
                    response.status(404).json("Cloudant response had no docs property.")
                }
            }
        })
    }
}