//
//  app.ts
//  Climate API
//
//  Created on October 15, 2016 by Animesh.
//  Copyright (c) 2016, Greenhill Sustainability
//  All rights reserved.
//

import { AppInfo }          from "./controller/AppInfo"
import { CloudantClient }   from "./controller/CloudantClient"
import { Letterpress }      from "./controller/Letterpress"
import { Routes }           from "./Routes"
import * as CloudFoundry    from "cfenv"
import * as BodyParser      from "body-parser"
import * as Express         from "express"
import * as Path            from "path"
import * as Logger          from "morgan"
import * as DotEnv          from "dotenv"

let app = Express()

//
// Middleware
//
app.use(Logger("dev"))
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({
  extended: true
}));

//
// Configuration
//
let appEnvironment = CloudFoundry.getAppEnv()
let appInfo = new AppInfo(appEnvironment.bind, appEnvironment.port, appEnvironment.url)

// When debugging locally, Cloud Foundry environment variables for "Sampled Data" database
// will be set to null (as there's no actual Cloud Foundy environment running locally).
// So to debug locally, we read credentials from a file ".env" at root. It's not checked 
// into version control (and should never be, would be a security risk), so you have to 
// create one locally and copy the credentials from Cloudant dashboard (speak to Animesh).
let credentials = appEnvironment.getServiceCreds("Sampled Data")
let dbURL: string 
if (credentials == null) {
    console.log("Reading local credentials...")
    DotEnv.config()
    dbURL = process.env.cloudantURL
}
else {
    dbURL = credentials.url
}

// 
// Let the games begin
//
let cloudant = new CloudantClient(dbURL, start)
function start(error?: any) {
    if (error) {
        console.log("Failed to initialize Cloudant database client. Here's what I know so far:")
        console.log(error.message)
    }
    else {
        Routes.Configure(app, appInfo, cloudant)
        app.listen(appInfo.port, appInfo.hostname, function() {
            console.log("Server running at http://" + appInfo.hostname + ":" + appInfo.port)
        })
    }
}