//
//  appInfo.ts
//  Climate API
//
//  Created on October 15, 2016 by Animesh.
//  Copyright (c) 2016, Greenhill Sustainability
//  All rights reserved.
//

export class AppInfo
{
    public readonly hostname: string
    public readonly port: number
    public readonly url: string
    public readonly name: string
    public readonly version: string
    public readonly description: string
    public readonly copyright: string

    public constructor(hostname?: string, port?: number, url?: string) {
        this.hostname = hostname
        this.port = port
        this.url = url
        this.name = "Climate API"
        this.version = "v1"
        this.description = "On-demand future climate projections for the United Kingdom"
        this.copyright = "2016 Greenhill Sustainability"
    }

    /**
     *  Checks whether the given version is supported by the API server.
     *  @param {string} version - The version identifier used in request URL.
     *                            E.g. "v1" in case of "https://abc.io/api/v1/def"
     *  @returns {boolean} Returns `true` if the API version is supported and live,
     *                     otherwise returns `false`. Use `AppInfo.getVersion()`
     *                     to get the latest API version. 
     */
    public DoesSupport(version: string): boolean {
        if (version == this.version) {
            return true
        }
        return false
    }
}

