//
//  CloudantClient.ts
//  Climate API
//
//  Created on October 15, 2016 by Animesh.
//  Copyright (c) 2016, Greenhill Sustainability
//  All rights reserved.
//

import * as Cloudant from "cloudant"

export class CloudantClient
{
    private _cloudant: any
    private _absDatabase: any
    private _cdfDatabase: any

    public constructor(uri: string, callback: (error?: any) => void)
    {
        let cloudant = Cloudant({ url: uri }, (error: any, cloudant: any) => {
            if (error) {
                callback(error)
            }
            else {
                console.log("Connection to Cloudant successful.")
                this._cloudant = cloudant
                this._absDatabase = cloudant.db.use("absolute")
                this._cdfDatabase = cloudant.db.use("cdf")
                callback()
            }
        })
    }

    /**
     *  Returns a list of databases stored in the Cloudant instance defined
     *  by the `_cloudant` property. 
     */
    public listDbs()
    {
        this._cloudant.db.list((error: any, dbs: any) => {
            if (error) {
                console.log("Ran into an error while trying to fetch all databases.")
                console.log(error.message)
            }
            else {
                console.log("List of databases: \n", dbs.join(", "))
            }
        })
    }

    public getDocumentWithID(id: string, callback: (data?: any, error?: any) => void) {
        this._absDatabase.get(id, (error: any, data: any) => {
            callback(data, error)
        })
    }

    public search(query: any, callback: (data?: any, error?: any) => void)
    {
        this._absDatabase.find(query, (error: any, results: any) => {
            if (error) {
                callback(null, "Couldn't find any documents matching the query.")
            }
            else {
                callback(results, null)
            }
        })
    }
}