//
//  letterpress.ts
//  Climate API
//
//  Created on October 15, 2016 by Animesh.
//  Copyright (c) 2016, Greenhill Sustainability
//  All rights reserved.
//

import * as Pug from "pug"
import { AppInfo } from "./AppInfo"

/**
 *  Use a Letterpress object to manage HTML templating
 *  across the app. Letterpress merely acts as a central
 *  place for managing template data objects, and delegates
 *  actual templating to `Pug`.
 */
export class Letterpress
{
    public static HomePage(appInfo: AppInfo): string {
        let html = Pug.compileFile("././view/home.pug")
        return html({
            name:           appInfo.name,
            description:    appInfo.description,
            hostname:       appInfo.url,
            version:        appInfo.version,
            copyright:      appInfo.copyright
        })
    }
}