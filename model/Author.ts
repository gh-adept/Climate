//
//  Author.ts
//  Climate API
//
//  Created on October 21, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

/**
 *  Name of data creators
 *  - MOHC  = Met Office Hadley Centre
 *  - NCL   = Newcastle University
 *  - UEA   = University of East Anglia
 *  - UKCIP = UK Climate Impacts Programme
 *  - BADC  = British Atmospheric Data Centre 
 */
export enum Author
{
    MOHC,
    NCL,
    UEA,
    UKCIP,
    BADC
}