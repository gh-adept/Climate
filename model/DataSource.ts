//
//  DataSource.ts
//  Climate API
//
//  Created on October 21, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

export enum DataSource 
{
    Land,
    Marine,
    StormSurge,
    SeaLevelRise
}