//
//  Frequency.ts
//  Climate API
//
//  Created on October 21, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

import { FrequencyType } from "./FrequencyType"

export class Frequency
{
    public type: FrequencyType
    /** Name of the month/season/year for which the projection is made */
    public name: string

    public constructor(type: FrequencyType, name: string) {
        this.type = type 
        this.name = name
    }
}