//
//  FrequencyType.ts
//  Climate API
//
//  Created on October 21, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

/**
 *  The temporal averaging frequency for the data. Can be 
 *  monthly, seasonal or annual. 
 */
export enum FrequencyType
{
    Month,
    Season,
    Annual
}

