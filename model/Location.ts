//
//  Location.ts
//  Climate API
//
//  Created on October 20, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

import { LocationType } from "./LocationType"

export class Location 
{
    public id: number
    public name: string
    public type: LocationType
    public latitude: number
    public longitude: number
   
    public constructor(type?: LocationType, id?: number, name?: string, latitude?: number, longitude?: number) 
    {
        // If location type is GridSquare, then an Grid Box ID must be set
        if (type == LocationType.GridSquare) {
            this.id = id
        }

        // If location type is a region, then a name for the region must be set 
        if (type == LocationType.Region) {
            this.name = name
        }
        
        this.type = type
        this.latitude = latitude
        this.longitude = longitude
    }
}