//
//  LocationType.ts
//  Climate API
//
//  Created on October 20, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

/**
 *  A location can be of type GridSquare, Marine or an aggregated
 *  region. 
 */
export enum LocationType 
{
    GridSquare,
    Marine,
    Region
}