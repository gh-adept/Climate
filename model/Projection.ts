//
//  Projection.ts
//  Climate API
//
//  Created on October 20, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

import { Author }           from "./Author"
import { DataSource }       from "./DataSource"
import { Frequency }        from "./Frequency"
import { Location }         from "./Location"
import { LocationType }     from "./LocationType"
import { ProjectionType }   from "./ProjectionType"
import { Sampling }         from "./Sampling"
import { SamplingType }     from "./SamplingType"
import { Scenario }         from "./Scenario"
import { TimePeriod }       from "./TimePeriod"
import { Variable }         from "./Variable"
import { Variant }          from "./Variant"

export class Projection
{
    /** Author of the data projections */
    public author: Author

    /** Climate model used for projections */
    public readonly model: string = "UKCP09 Models"

    /** Project name */
    public readonly project: string = "Adept by Greenhill Sustainability"

    /** Data source for the projection. Can be land, marine or sea level rise. */
    public dataSource: DataSource

    /** Type of projection. Absolute or change-only */
    public projectionType: ProjectionType

    /** Emissions scenario */
    public scenario: Scenario

    /** Time period  */
    public timePeriod: TimePeriod

    /** Frequency */
    public frequency: Frequency

    /** Location */
    public location: Location

    /** Defines how a subset was sampled from the set of 10,000 variants */
    public sampling: Sampling

    /** Climate variable */
    public variable: Variable

    /** Projected variants */
    public variants: Array<Variant>

    public constructor(author: Author, dataSource: DataSource, projectionType: ProjectionType) 
    {
        this.author = author
        this.dataSource = dataSource
        this.projectionType = projectionType
    }
}


