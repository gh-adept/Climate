//
//  ProjectionType.ts
//  Climate API
//
//  Created on October 21, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

/**
 *  Climate change predictions are reported in two ways - absolute future climate 
 *  values and predicted changes in climate relative to a 1961-1990 baseline.
 */
export enum ProjectionType
{
    Absolute,
    ChangeOnly
}