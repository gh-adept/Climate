//
//  Sampling.ts
//  Climate API
//
//  Created on October 21, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

import { SamplingType } from "./SamplingType"

export class Sampling 
{
    public type: SamplingType

    // Number of random samples. Applicable only when sampling method is 'random'
    public count: number

    // Percentiles the data was sampled at, only applicable when sampling method is 'by percentile'
    public percentiles: Array<number>

    public constructor(type: SamplingType, percentiles?: Array<number>, count?: number) 
    {
        this.type = type 
        if (type == SamplingType.ByPercentile) {
            this.count = 3
            if (percentiles != undefined) {
                this.percentiles = percentiles
            }
            else {
                this.percentiles = [10, 50, 90]
            }
        }
        else {
            this.count = count
        }
    }
}