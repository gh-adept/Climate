//
//  SamplingType.ts
//  Climate API
//
//  Created on October 21, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

/**
 *  The sampling method employed to pick a subset from the 10,000 odd variants in a given climate variable projection.
 */
export enum SamplingType
{
    SelectAll,
    Random,
    ByID,
    ByPercentile
}