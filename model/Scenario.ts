//
//  Scenario.ts
//  Climate API
//
//  Created on October 21, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

/**
 *  Emissions Scenarios are defined as per the IPCC SRES standards as follows:
 *  - **Low**, B1 
 *  - **Medium**, A1B
 *  - **High**, A1FI
 */
export enum Scenario
{
    Low,
    Medium,
    High
}