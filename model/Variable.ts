//
//  Variable.ts
//  Climate API
//
//  Created on October 20, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

import { VariableType } from "./VariableType"

export class Variable
{
    public type: VariableType
    public name: string
    /** Unit for reporting absolute values */
    public absoluteUnit: string
    /** Unit for reporting change values */
    public changeUnit: string

    public constructor(type: VariableType, name?: string, absoluteUnit?: string, changeUnit?: string) 
    {
        this.type = type
        this.name = name 
        this.absoluteUnit = absoluteUnit
        this.changeUnit = changeUnit
    }
}