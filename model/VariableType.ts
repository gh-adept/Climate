//
//  VariableType.ts
//  Climate API
//
//  Created on October 20, 2016 by Animesh.
//  Copyright (c) 2016 Greenhill Sustainability
//  All rights reserved.
//

export enum VariableType
{
    MeanDailyTemp,
    MeanDailyMaxTemp,
    MeanDailyMinTemp,
    Precipitation,
    SpecificHumidity,
    RelativeHumidity,
    TotalCloud,
    NetSurfaceLongWaveFlux,
    NetSurfaceShortWaveFlux,
    TotalDownwardShortWaveFlux,
    MeanSeaLevelPressure,
    MeanDailyAirTemp
}