The Climate Projections API serves UKCP09 data projections on demand via a RESTful API.

## What is UKCP09?

> UKCP09 is the name given to the UK Climate Projections. The UKCP09 website allow users to access information on plausible changes in 21st century climate for the United Kingdom. UKCP09 provides future climate projections for land and marine regions as well as observed (past) climate data for the UK.

> UKCP09 was produced in 2009, funded by a number of agencies led by Defra. It is based on sophisticated scientific methods provided by the Met Office, with input from over 30 contributing organisations. UKCP09 can be used to help organisations assess potential impacts of the projected future climate and to explore adaptation options to address those impacts.

> The UKCP09 website is managed by the Environment Agency working with the Met Office.

## API Request parameters

This server expects an API call in the following format:
```
/api/<version>/<locationID>/<period>/<temporalAveraging>/<variable>/<scenarios>
```

The code and API documentation uses many abbreviations. These are described below.

### Data Source
Term used to describe the different types of information provided by UKCP09. 

* **PrLnd**   - prob_land
* **PrMar**   - prob_marine
* **WG**      - wxgen
* **MLMar**   - marine_model
* **StSrg**   - storm_surge
* **SLev**    - sea_level_rise

### Batch
A group of variables that, within UKCP09, have been processed together for each
location. 
Some users wish to examine joint-probabilities between multiple variables. The
method that generates the sampled data incorporates a multivariate analysis that can
only process a certain number of variables simultaneously. This limitation means
that the variables have been processed in two separate batches. Examining
joint-probabilities between variables in different batches is not recommended.

* **Bat1** - Batch 1
* **Bat2** - Batch 2

### Climate Change Type
The climate change type refers to whether the values are provided as the change
relative to the baseline climate or the absolute future climate values.

* **Chg**     - Future climate change only
* **Abs**     - Absolute future climate change

### Variable
The name given to quantities for which the observed trends are described or future
changes are projected in UKCP09. These include climate variables (temperature,
precipitation, etc.), marine variables (sea level rise, salinity, etc.) and derived
variables (cooling degree days, days of air frost etc.).

* **Tmean**       - Mean temperature (°C)
* **Tmax**        - Mean daily maximum temperature (°C)
* **Tmin**        - Mean daily minimum temperature (°C)
* **CoolD**       - Temperature of the coolest day (°C)
* **WarmD**       - Temperature of the warmest day (°C)
* **ColdN**       - Temperature of the coolest night (°C)
* **WarmN**       - Temperature of the warmest night (°C)
* **Pmean**       - Precipitation (%)
* **Wet**         - Precipitation on the wettest day (%)
* **SLP**         - Mean sea level pressure (hPa)
* **Clo**         - Total Cloud (%)
* **Rhum**        - Relative humidity (%)
* **Shum**        - Specific humidity (%)
* **NSLWF**       - Net surface longwave flux (W/m2)
* **NSSWF**       - Net surface shortwave flux (W/m2)
* **TDSSWF**      - Total downward surface shortwave flux (W/m2)
* **SkSrgTrd**    - Long-term trend in skew surge (1951-2099)
* **ASLR**        - Absolute Sea Level Rise
* **RSLR**        - Relative Sea Level Rise

### Emissions Scenario
A plausible representation of the future development of emissions of substances
(e.g. greenhouse gases and aerosols) that can affect the radiative balance of the
globe. 
These representations are based on a coherent and internally consistent set of
assumptions about determining factors (such as demographic and socio-economic
development, technological change) and their key relationships. 
The emissions scenarios used in UKCP09 do not include the effects of planned
mitigation policies, but do assume different pathways of technological and economic
growth which include a switch from fossil fuels to renewable sources of energy.

* **Lo**          - Low
* **Med**         - Medium
* **Hi**          - High

### Time Period
A period of 30 years over which climate averages are calculated.

* **2020**        - 2010-2039
* **2030**        - 2020-2049
* **2040**        - 2030-2059
* **2050**        - 2040-2069
* **2060**        - 2050-2079
* **2070**        - 2060-2089
* **2080**        - 2070-2099

### Temporal Average
The time over which the variables in UKCP09 are averaged, i.e. month, season or
annual.

* **Jan**         - January
* **Feb**         - February
* **Mar**         - March
* **Apr**         - April
* **May**         - May
* **Jun**         - June
* **Jul**         - July
* **Aug**         - August
* **Sep**         - September
* **Oct**         - October
* **Nov**         - November
* **Dec**         - December
* **Win**         - Winter (December, January, February)
* **Spr**         - Spring (March, April, May)
* **Sum**         - Summer (June, July, August)
* **Aut**         - Autumn (September, October, November)
* **Ann**         - Annual

### Location
The geographic place of interest for which a projection is provided.When a numeric
code is provided this represents the grid box ID as on [this page](http://www.metoffice.gov.uk/climatechange/science/monitoring/ukcp09/download/gridMaps/25kmMapSearch.html).

* **CH**          - Channel Islands
* **EM**          - East Midlands
* **EOE**         - East of England
* **SCE**         - Eastern Scotland
* **IOM**         - Isle of Man 
* **LON**         - London
* **NE**          - North East England
* **NW**          - North West England
* **NI**          - Northern Ireland
* **SCN**         - Northern Scotland
* **SE**          - South East England
* **SW**          - South West England
* **WAL**         - Wales
* **WM**          - West Midlands
* **SCW**         - Western Scotland 
* **YH**          - Yorkshire and Humberside 
* **ANG**         - Anglian
* **ARG**         - Argyll
* **CLY**         - Clyde
* **DEE**         - Dee
* **FOR**         - Forth 
* **HUB**         - Humber 
* **BAN**         - Neagh Bann
* **NEI**         - North East Ireland
* **NES**         - North East Scotland 
* **HIG**         - North Highland 
* **NWE**         - North West England
* **NWI**         - North West Ireland 
* **NBR**         - Northumbria 
* **ORK**         - Orkney and Shetland
* **SEV**         - Severn 
* **SOL**         - Solway 

### Probability Data Type
In the case of probabilistic projections over land or marine regions the probability
data type refers to whether the data is represented as the full sampled data or a
cumulative probability distribution function (CDF).

* **Samp**        - Sampled Data
* **CDF**         - Cumulative Distribution Function